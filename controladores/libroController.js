'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express()

var Libro = require('../modelos/libro');

//crear libro
function crearLibro(req, res) {

    let libro = new Libro()
    libro.codigo = req.body.codigo
    libro.nombre = req.body.nombre
    libro.autor = req.body.autor
    libro.cantidad = req.body.cantidad

    libro.save((err, libroStore) => {

        if (err) res.status(500).send(`Error base de datos> ${err}`)

        res.status(200).send({ libro: libroStore })

    })

}


//modificar libro
function modificarLibro(req, res) {

    let LibroId = req.params.LibroId;
    let body = req.body;

    Libro.findByIdAndUpdate(LibroId, body, { new: true }, (err, libro) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            libro
        });

    });

}

//borrar libro
function borrarLibro(req, res) {

    let LibroId = req.params.libroId
    Libro.findByIdAndDelete(LibroId, (err, libro) => {
        if (err) return res.status(500).send({ message: 'error al realizar petición' })
        if (!libro) return res.status(404).send({ message: 'Error el libro no existe' })
        res.status(200).send({ message: '¡el libro se borró con éxito!' })

    })

}

//listar libro
function listarLibro(req, res) {

    Libro.find({}).then(function(libro) {
        res.send({ libro });
    });

}

//exportamos las funciones en un objeto json
module.exports = {

    crearLibro,
    modificarLibro,
    borrarLibro,
    listarLibro

};