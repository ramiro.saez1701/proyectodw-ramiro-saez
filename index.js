'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Libro = require('./modelos/libro')
var libroController = require('./controladores/libroController');

const app = express()

/*
app.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Authorization, X-API-KEY, Origin, X-Requested-With');
    res.header('Access-Control-Allow-Methods','GET,POST,OPTIONS,PUT,DELETE');
    res.header('Allow','GET,POST,OPTIONS,PUT,DELETE');
    next();
});*/

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.post('/libro', libroController.crearLibro);
app.put('/modlibro', libroController.modificarLibro);
app.delete('/borrarlibro', libroController.borrarLibro);
app.get('/libros', libroController.listarLibro);

app.get('/hola', (req, res) => {

    res.status(200).send({ message: "bienvenido" })

})

app.get('/api/libro', (req, res) => {

    res.status(200).send('Aquí devolveremos los libros')

})

app.get('/api/libro/:libroId', (req, res) => {

    let LibroId = req.params.libroId
    Libro.findById(LibroId, (err, libro) => {
        if (err) return res.status(500).send({ message: 'error al realizar petición' })
        if (!libro) return res.status(404).send({ message: 'Error el libro no existe' })
        res.status(200).send({ libro })
    })

})

app.post('/api/libro', (req, res) => {

    let libro = new Libro()
    libro.codigo = req.body.codigo
    libro.nombre = req.body.nombre
    libro.autor = req.body.autor
    libro.cantidad = req.body.cantidad

    libro.save((err, libroStore) => {

        if (err) res.status(500).send(`Error base de datos> ${err}`)

        res.status(200).send({ libro: libroStore })

    })

})

app.put('/api/libro/:LibroId', (req, res) => {

    let LibroId = req.params.LibroId;
    let body = req.body;

    Libro.findByIdAndUpdate(LibroId, body, { new: true }, (err, libro) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            libro
        });

    });

});

app.delete('/api/libro/:libroId', (req, res) => {

    let LibroId = req.params.libroId
    Libro.findByIdAndDelete(LibroId, (err, libro) => {
        if (err) return res.status(500).send({ message: 'error al realizar petición' })
        if (!libro) return res.status(404).send({ message: 'Error el libro no existe' })
        res.status(200).send({ message: '¡el libro se borró con éxito!' })

    })

})

mongoose.connect('mongodb+srv://mephi:lupus123xD@cluster0-mzp6v.mongodb.net/test?retryWrites=true&w=majority', (err, res) => {

    if (err) throw err
    console.log('Conexion establecida')

    app.listen(2000, () => {

        console.log("Está corriendo en el puerto 2000")

    })

})